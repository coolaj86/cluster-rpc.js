'use strict';

module.exports.create = function (opts) {
  //var msgPrefix = 'cluster-rpc.' + opts.name;
  //var rpcPrefix = msgPrefix + '.rpc';
  //var resultPrefix = msgPrefix + '.result';
  //var initPrefix = msgPrefix + '.init';
  var root = 'com.daplie.cluster-rpc.' + (opts.name ? opts.name + '.' : '');
  return {
    root: root
  , rpc: root + 'rpc'
  , result: root + 'result'
  , init: root + 'init'
  , connect: root + 'connect'
    // TODO the things that are using this should probably accept opts
  , debug: opts.debug
  };
};
