'use strict';

module.exports.create = function (prefixes) {
  if (prefixes.debug) { console.log('[cluster-rpc] master created'); }
  var m = new (require('events').EventEmitter)();

  m.addWorker = function (worker) {
    if (prefixes.debug) { console.log('[cluster-rpc] [master] adding worker'); }
    m._workers = [];

    var w = new (require('events').EventEmitter)();

    function emitConnection() {
      if (w.__online) {
        return;
      }

      w.__online = true;
      m.emit('connection', w);
    }

    worker.on('online', function () {
      if (prefixes.debug) { console.log('[cluster-rpc] [master] worker came online, at fork'); }
      emitConnection();
    });

    worker.on('message', function (data) {
      if (prefixes.connect === data.type) {
        if (prefixes.debug) { console.log('[cluster-rpc] [master] worker connected, manually'); }
        emitConnection();
        return;
      }
      if (prefixes.debug) { console.log('[cluster-rpc] [master] worker sent message', data); }
      w.emit('message', data);
    });

    w.send = function (data) {
      if (prefixes.debug) { console.log('[cluster-rpc] [master] sending', data); }
      worker.send(data);
    };

    // TODO remove workers that exit
    m._workers.push(w);
  };

  return m;
};
