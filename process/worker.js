'use strict';

module.exports.create = function (process, prefixes) {
  if (prefixes.debug) { console.log('[cluster-rpc] worker created'); }
  var w = new (require('events').EventEmitter)();

  process.on('message', function (data) {
    w.emit('message', data);
  });

  w.send = function (data) {
    process.send(data);
  };

  // if this were a web / unix socket there would be a 'connection' event
  // emulating this is useful since the worker may create its cluster rpc
  // at any time, (which means it may miss the 'fork' event)
  w.send({ type: prefixes.connect });

  return w;
};
