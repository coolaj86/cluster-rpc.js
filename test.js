'use strict';

var cluster = require('cluster');
var crpc;

function runMaster() {

  var db = {
    get: function (key, cb) {
      cb(null, db[key]);
    }
  , put: function (key, val, cb) {
      db[key] = val;
      if (cb) { cb(null); }
    }
  };

  crpc = require('./master').create({
    instance: db
  , methods: [ 'get', 'put' ]
  , name: 'foo-level'
  });
  crpc.then(function () {
    db.put('foo', 'bar');
  });
  cluster.fork();

}

function runWorker() {

  crpc = require('./worker').create({
    name: 'foo-level'
  });


}

if (cluster.isMaster) {

  runMaster();

}
else {

  runWorker();

}


crpc.then(function (db) {
  setTimeout(function () {
    db.get('foo', function (err, result) {
      console.log(cluster.isMaster && '0' || cluster.worker.id.toString(), "db.get('foo')", result);

      if (!cluster.isMaster) {
        process.exit(0);
      }
    });
  }, 250);
});
