'use strict';

console.error("");
console.error("One does not simply require('cluster-rpc');");
console.error("");
console.error("Usage:");
console.error("\trequire('cluster-rpc/master').create({ instance: ..., methods: ... name: ... });");
console.error("\trequire('cluster-rpc/worker').create({ name: ... });");
console.error("");
console.error("");

process.exit(1);
